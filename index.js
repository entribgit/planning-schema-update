"use strict";

//logger 
const bunyan = require('bunyan');
const log = bunyan.createLogger({ name: 'planning_schema_update', level: 20 });
const moment = require('moment');
const _ = require('lodash');
const config = require('./config.json');
const schema = require('./schema.json');
const entribrequest = require('./entrib-request/request.js');
let clearFunctionCall;
getPlanningSchema();

/*
    update schema with original schema in schema.json file. 
*/
function updateSchema(){
    let path = "/server/elements/"+config.elementName+"?customerid=" + config.customer.customerid + "&userid=" + config.customer.userid;
    let request = new entribrequest(); 
    let time = moment().format("DD-MM-YYYY:HH:mm:ss");
    log.info(time , " ", path);          
    request.getEventemitter().on('response', function (response) {
        try{
            let data =  JSON.parse(response.data)
            log.info(time , " Original Schema updated ",data);
        } catch(exception){
            log.error(time , " Exception in updating schema ",exception);
        }
    });

    request.getEventemitter().on('requesterror', function (error) {
       log.error(time , " ",error);
    });
    let postData = JSON.stringify(schema);
    request.putRequest(config.server.host, config.server.port, path,postData);
}

/*
    check schema is changed or not if change then update original schema
*/

function checkSchemaChange(data) {
    let questions =  data.questions;
    let isTrue = _.isEqual(questions, schema.questions);
    let time = moment().format("DD-MM-YYYY:HH:mm:ss");
    log.info(time , " Schema Status ", isTrue);
    let changes = [];
    if(!isTrue){
        log.info(time , " Schema Changed");
        log.info(time , " mongo Schema",data);
        updateSchema();
    }
    
}

/*
    getPlanning Schema from mongo
*/
function getPlanningSchema (data) {
    let path = "/server/elements/"+config.elementName+"?customerid=" + config.customer.customerid + "&userid=" + config.customer.userid;
    let request = new entribrequest(); 
    // clear existing function call
    clearTimeout(clearFunctionCall)
    request.getEventemitter().on('response', function (response) {
        let data = {};
        let time = moment().format("DD-MM-YYYY:HH:mm:ss");
        try{
            if(response.statusCode == 200){
                log.info(time, " GOT Mongo Schema");
                data =  JSON.parse(response.data).results
                checkSchemaChange(data);
            } else {
                log.error(time , " Status Code " + response.statusCode)
                log.error(time , " ",response);
            }
        } catch(exception){
            log.error(time , " ", response);
            log.error(time , " Exception in parsing schema ",exception);
        } finally{
            // call getPlanningSchema function after setTimout 
            clearFunctionCall = setTimeout(function(){
                getPlanningSchema()
            },config.default.interval * 1000)
        }
    });

    request.getEventemitter().on('requesterror', function (error) {
        let time = moment().format("DD-MM-YYYY:HH:mm:ss");
        log.error(time , " ", error);
        // call getPlanningSchema function after setTimout 
        clearFunctionCall = setTimeout(function(){
            getPlanningSchema()
        },config.default.interval * 1000)
    });
    request.getRequest(config.server.host, config.server.port, path);
}